const webpack = require("webpack"),
  path = require("path"),
  HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = function (env) {
  return {
    mode: env.development ? "development" : "production",
    context: path.resolve(__dirname, "../src"),
    entry: ["./styles.css", "./index.html", "./main.js"],
    output: {
      publicPath: "/",
      path: path.resolve(__dirname, "../dist"),
      filename: "bundle.js"
    },
    resolve: {
      extensions: [".js"],
      modules: [
        path.resolve(__dirname, "../src"),
        path.resolve(__dirname, "../node_modules")
      ]
    },
    resolveLoader: {
      modules: [path.resolve(__dirname, "../node_modules")]
    },
    module: {
      rules: [{
          test: /\.html$/,
          use: ["html-loader"],
          include: [path.resolve(__dirname, "../src")]
        },
        {
          test: /\.(eot|svg)$/,
          use: ["file-loader?name=[name].[hash:20].[ext]"]
        },
        {
          test: /\.(jpg|png|gif|otf|ttf|woff|woff2|cur|ani)$/,
          use: ["url-loader?name=[name].[hash:20].[ext]&limit=10000"]
        }
      ]
    },
    plugins: [
      new webpack.NoEmitOnErrorsPlugin(),
      new HtmlWebpackPlugin({
        template: "./index.html",
        inject: "body"
      })
    ]
  };
};