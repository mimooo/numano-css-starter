const webpack = require("webpack"),
  webpackMerge = require("webpack-merge"),
  commonConfig = require("./webpack.common"),
  path = require("path"),
  ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = function(env) {
  return webpackMerge(commonConfig(env), {
    module: {
      rules: [
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: ["css-loader?importLoaders=1", "postcss-loader"]
          }),
          include: [path.resolve(__dirname, "../src")]
        }
      ]
    },
    plugins: [new ExtractTextPlugin("styles.css")]
  });
};
