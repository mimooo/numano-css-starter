module.exports = {
  plugins: {
    "postcss-import": {},
    "postcss-url": {},
    "rucksack-css": {
      reporter: true
    },
    "postcss-css-variables": {},
    // "postcss-custom-properties": {},
    "postcss-apply": {},
    "postcss-calc": {},
    "postcss-nesting": {},
    "postcss-custom-media": {},
    "postcss-media-minmax": {},
    "postcss-custom-selectors": {},
    "postcss-attribute-case-insensitive": {},
    "postcss-color-rebeccapurple": {},
    "postcss-color-hwb": {},
    "postcss-color-gray": {},
    "postcss-color-hex-alpha": {},
    "postcss-color-function": {},
    "postcss-font-family-system-ui": {},
    "postcss-font-variant": {},
    "pleeease-filters": {},
    "postcss-initial": {},
    pixrem: {},
    "postcss-selector-matches": {},
    "postcss-selector-not": {},
    "postcss-pseudo-class-any-link": {},
    autoprefixer: {},
    "postcss-browser-reporter": {}
  }
};

// npm i -D postcss-import postcss-url rucksack-css postcss-css-variables postcss-custom-properties postcss-apply postcss-calc postcss-nesting postcss-custom-media postcss-media-minmax postcss-custom-selectors postcss-attribute-case-insensitive postcss-color-rebeccapurple postcss-color-hwb postcss-color-gray postcss-color-hex-alpha postcss-color-function postcss-font-family-system-ui postcss-font-variant pleeease-filters postcss-initial pixrem postcss-selector-matches postcss-selector-not postcss-pseudo-class-any-link autoprefixer postcss-browser-reporter;